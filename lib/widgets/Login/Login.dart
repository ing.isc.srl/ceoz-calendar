import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:io';

import 'package:frontend/widgets/CircularProgressBar/CircularProgressBar.dart';
import 'package:frontend/widgets/Calendar/Calendar.dart';
import 'package:frontend/provider/index.dart';
import 'package:frontend/storage/index.dart';
import 'package:frontend/jwt/index.dart';
import 'package:frontend/svg/logo.dart';
import 'TextInput.dart';

import 'package:frontend/graphql/Mutations.dart' as Mutations;
import 'package:frontend/constants.dart' as Constants;

class Login extends StatefulWidget {
  final Storage storage;
  Login({Key key, this.storage}) : super(key: key);
  LoginState createState() => LoginState();
}

class LoginState extends State<Login> {
  final TextEditingController usernameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  Future<File> writeJWT(String data) => widget.storage.writeJWT(data);

  void dispose() {
    usernameController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  void proceed(int role) {
    Provider.of<States>(context, listen: false).setIsAdmin =
        role == 0 ? true : false;
    Navigator.of(context).pushReplacement(
        MaterialPageRoute<void>(builder: (BuildContext context) => Calendar()));
  }

  void showCircularProgress() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) =>
            CupertinoDialogAction(child: CircularProgressBar()));
  }

  void onPressed(RunMutation runMutation) {
    if (formKey.currentState.validate()) {
      showCircularProgress();
      runMutation({
        'profesionalID': usernameController.text,
        'password': passwordController.text
      });
    }
  }

  void onCompleted(dynamic res) {
    Navigator.of(context).pop();
    final String data = res['login'];
    try {
      final Map<String, dynamic> payload = parseJwt(data);
      writeJWT(data);
      Provider.of<States>(context, listen: false).setJwtoken = data;
      proceed(payload['rol']);
    } catch (e) {
      Scaffold.of(context).showSnackBar(SnackBar(content: Text(data)));
    }
  }

  Mutation signInButton() => Mutation(
      options: MutationOptions(
          documentNode: gql(Mutations.LOGIN), onCompleted: onCompleted),
      builder: (RunMutation runMutation, QueryResult result) {
        return RaisedButton(
          onPressed: () => onPressed(runMutation),
          child: Text('INGRESAR'),
        );
      });

  Form loginForm() => Form(
        key: formKey,
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20.0),
              child: Column(
                children: <Widget>[
                  TextInput(type: "username", controller: usernameController),
                  TextInput(type: "password", controller: passwordController)
                ],
              ),
            ),
            signInButton()
          ],
        ),
      );

  LayoutBuilder layoutBuilder(List<Widget> children) => LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) =>
            SingleChildScrollView(
                child: ConstrainedBox(
          constraints: BoxConstraints(minHeight: constraints.maxHeight),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: children),
        )),
      );

  Widget build(BuildContext context) {
    final EdgeInsets loginPadding =
        EdgeInsets.symmetric(vertical: 80.0, horizontal: 25.0);

    return Scaffold(
        backgroundColor: Constants.MAIN_COLOR,
        body: layoutBuilder([
          logoCeoz(),
          Padding(
            padding: loginPadding,
            child: loginForm(),
          ),
        ]));
  }
}
