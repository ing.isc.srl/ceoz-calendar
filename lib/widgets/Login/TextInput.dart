import 'package:flutter/material.dart';

class TextInput extends StatefulWidget {
  final TextEditingController controller;
  final String type;
  TextInput({Key key, this.type, this.controller}) : super(key: key);
  TextInputState createState() => TextInputState();
}

class TextInputState extends State<TextInput> {
  Icon visibility = Icon(Icons.visibility_off, color: Colors.white);
  bool iconVisibilityPressed = false;

  void onPressed() {
    setState(() {
      if (iconVisibilityPressed) {
        visibility = Icon(Icons.visibility_off, color: Colors.white);
      } else {
        visibility = Icon(Icons.visibility, color: Colors.white);
      }
      iconVisibilityPressed = !iconVisibilityPressed;
    });
  }

  String validator(String value) {
    if (value.isEmpty)
      return widget.type == "password"
          ? 'Ingrese contraseña'
          : 'Ingrese usuario:';
    if (widget.controller.text.length < 8 && widget.type == 'password')
      return "Mínimo 8 carácteres";
    return null;
  }

  InputDecoration decoration() {
    final UnderlineInputBorder border =
        UnderlineInputBorder(borderSide: BorderSide(color: Colors.white));
    final TextStyle errorStyle = TextStyle(color: Colors.pink[100]);
    final String text = widget.type == "password" ? 'Contraseña' : 'Usuario';
    final TextStyle textStyle = TextStyle(color: Colors.white);
    final IconButton suffixIcon = widget.type != "password"
        ? null
        : IconButton(
            icon: visibility,
            onPressed: onPressed,
          );

    return InputDecoration(
        enabledBorder: border,
        errorBorder: border,
        errorStyle: errorStyle,
        focusedErrorBorder: border,
        labelText: '$text:',
        labelStyle: textStyle,
        hintText: text,
        hintStyle: textStyle,
        suffixIcon: suffixIcon);
  }

  Widget build(BuildContext context) {
    final bool obscureText =
        (widget.type == "password" && !iconVisibilityPressed) ? true : false;
    final TextStyle style = TextStyle(
        fontSize: 18.0, color: Colors.white, decorationColor: Colors.white);

    return TextFormField(
      obscureText: obscureText,
      controller: widget.controller,
      decoration: decoration(),
      validator: validator,
      style: style,
    );
  }
}
