import 'package:provider/provider.dart';
import 'package:flutter/material.dart';

import 'package:frontend/widgets/Login/Login.dart';
import 'package:frontend/widgets/Calendar/Calendar.dart';
import 'package:frontend/provider/index.dart';
import 'package:frontend/storage/index.dart';
import 'package:frontend/jwt/index.dart';

class Init extends StatefulWidget {
  final Storage storage;
  Init({Key key, this.storage}) : super(key: key);
  InitState createState() => InitState();
}

class InitState extends State<Init> {
  bool redirectCalendar = false;

  void initState() {
    super.initState();
    widget.storage.readJWT().then((String jwt) {
      try {
        final Map<String, dynamic> payload = parseJwt(jwt);
        final DateTime date =
            new DateTime.fromMillisecondsSinceEpoch(payload['exp'] * 1000);
        final DateTime now = new DateTime.now();
        final int difference = date.difference(now).inSeconds;

        Provider.of<States>(context, listen: false).setJwtoken = jwt;
        Provider.of<States>(context, listen: false).setIsAdmin =
            payload['rol'] == 0 ? true : false;

        if (difference > 0) redirectCalendar = true;
      } catch (e) {}
    });
  }

  Widget build(BuildContext context) =>
      redirectCalendar ? Calendar() : Login(storage: Storage());
}
