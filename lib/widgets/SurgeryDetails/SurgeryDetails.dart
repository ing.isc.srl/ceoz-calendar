import 'package:flutter/scheduler.dart';
import 'package:frontend/widgets/CircularProgressBar/CircularProgressBar.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:frontend/provider/index.dart';
import 'package:frontend/svg/eyeScalpel.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';

import 'package:frontend/graphql/Mutations.dart' as Mutations;
import 'package:frontend/graphql/Queries.dart' as Queries;
import 'package:frontend/constants.dart' as Constants;

class SurgeryDetails extends StatefulWidget {
  final String surgeryID;
  SurgeryDetails({Key key, this.surgeryID}) : super(key: key);
  SurgeryDetailsState createState() => SurgeryDetailsState();
}

class SurgeryDetailsState extends State<SurgeryDetails> {
  Map<String, String> sortData(Map<String, dynamic> surgeryData) {
    final String duration =
        '${surgeryData['duration']} ${surgeryData['duration'] == 1 ? "hora" : "horas"}';

    return {
      'Cirugía: ': surgeryData['name'],
      'Duración: ': duration,
      'Anestesia: ': surgeryData['anesthesia'],
      'Precio: ': '\$${surgeryData['price']}',
      'Folio: ': surgeryData['serial'],
      'Paciente: ': surgeryData['patientName'],
      '¿Se necesita lente intraocular? ': surgeryData['needLens'] ? "Si" : "No",
      '¿Que dioptria es el lente? ': surgeryData['dioptria'],
      '¿Es flexible o PMMA? ': surgeryData['flexibleOrPMMA'],
      '¿Se necesita silicon? ': surgeryData['needSilicon'] ? "Si" : "No",
      '¿Que otro insumo se necesita? ': surgeryData['otherThing'],
      'Estatus: ': surgeryData['status']
    };
  }

  Future<bool> showMessage(String message) async => await showDialog(
      context: context,
      builder: (BuildContext context) => SimpleDialog(
            title: Text(
              message,
              style: TextStyle(fontSize: 20.0),
            ),
            children: <Widget>[
              SimpleDialogOption(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: RaisedButton(
                    onPressed: null,
                    disabledColor: Constants.MAIN_COLOR,
                    child: Text(
                      'Aceptar',
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white, fontSize: 25.0),
                    ),
                  )),
            ],
          ));

  void onCompleted(dynamic res) async {
    bool res = await showMessage('Cirugía aceptada.');
    if (res ?? true) Navigator.of(context).pop();
  }

  Padding eyeScalpelSVG() => Padding(
        padding: EdgeInsets.symmetric(vertical: 40.0),
        child: eyeScalpel(),
      );

  Padding dataRow(String leading, String content) => Padding(
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 25.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(leading,
              style: TextStyle(fontSize: 20.0, color: Constants.MAIN_COLOR)),
          Flexible(child: Text(content, style: TextStyle(fontSize: 20.0)))
        ],
      ));

  Mutation approveSurgery(String id) => Mutation(
      options: MutationOptions(
          documentNode: gql(Mutations.APPROVE_SURGERY),
          onCompleted: onCompleted),
      builder: (RunMutation runMutation, QueryResult queryResult) {
        return RaisedButton(
            child: Text(
              'Aceptar cirugía',
              style: TextStyle(fontSize: 20.0),
            ),
            textColor: Colors.white,
            color: Constants.MAIN_COLOR,
            onPressed: () => runMutation({'id': id}));
      });

  Query querySurgeryData(String surgeryID) {
    final bool isAdmin = Provider.of<States>(context).getIsAdmin;

    return Query(
        options: QueryOptions(
            documentNode: gql(Queries.SURGERY), variables: {'id': surgeryID}),
        builder: (QueryResult res,
            {VoidCallback refetch, FetchMore fetchMore}) {
          if (res.hasException) return Text(res.exception.toString());
          if (res.loading) return CircularProgressBar();

          Map<String, String> information = sortData(res.data['surgery']);
          List<Widget> info = List<Widget>();

          information.forEach(
              (leading, content) => info.add(dataRow(leading, content)));

          return Column(
              children: <Widget>[
            eyeScalpelSVG(),
            Center(
                child: Wrap(
              children: info,
              spacing: 8.0,
            )),
            isAdmin && res.data['surgery']['status'] == "Pendiente"
                ? Center(child: approveSurgery(surgeryID))
                : null
          ].where((Object o) => o != null).toList());
        });
  }

  LayoutBuilder layout(Widget child) => LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constrains) =>
          SingleChildScrollView(
              child: ConstrainedBox(
                  constraints: BoxConstraints(minHeight: constrains.maxHeight),
                  child: Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 10.0),
                      child: child))));

  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Detalles de cirugía'),
          backgroundColor: Constants.MAIN_COLOR,
        ),
        body: layout(querySurgeryData(widget.surgeryID)));
  }
}
