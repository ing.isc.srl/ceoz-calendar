import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:provider/provider.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:io';

import 'package:frontend/widgets/CircularProgressBar/CircularProgressBar.dart';
import 'package:frontend/widgets/ModalChangePassword/ModalChangePass.dart';
import 'package:frontend/widgets/DoctorDetails/DoctorDetails.dart';
import 'package:frontend/widgets/SurgeriesList/SurgeriesList.dart';
import 'package:frontend/widgets/Login/Login.dart';
import 'TableCalendar.dart';

import 'package:frontend/provider/index.dart';
import 'package:frontend/storage/index.dart';
import 'package:frontend/jwt/index.dart';

import 'package:frontend/graphql/Subscriptions.dart' as Subscriptions;
import 'package:frontend/graphql/Mutations.dart' as Mutations;
import 'package:frontend/graphql/Queries.dart' as Queries;
import 'package:frontend/constants.dart' as Constants;

class Calendar extends StatefulWidget {
  CalendarState createState() => CalendarState();
}

class CalendarState extends State<Calendar> {
  final CalendarController calendarCtrl = CalendarController();
  final Storage storage = new Storage();
  bool isAdmin = false;

  void initState() {
    super.initState();
    isAdmin = Provider.of<States>(context, listen: false).getIsAdmin;
  }

  void dispose() {
    calendarCtrl.dispose();
    super.dispose();
  }

  void profile() {
    Navigator.of(context).push(MaterialPageRoute<void>(
        builder: (BuildContext context) => DoctorDetail()));
  }

  void redirectToLogin() {
    Navigator.of(context).pushReplacement(MaterialPageRoute<void>(
        builder: (BuildContext context) => Login(storage: Storage())));
  }

  void handleSurgeriesListIconPressed() {
    Navigator.of(context).push(MaterialPageRoute<void>(
        builder: (BuildContext context) =>
            SurgeriesList(title: 'Cirugías programadas')));
  }

  void handleAccountIconPressed() {
    if (isAdmin)
      showDialog(context: context, builder: (_) => ModalChangePass());
    else
      profile();
  }

  void handleLogoutIconPressed(RunMutation runMutation) {
    runMutation({});
    redirectToLogin();
    writeJWT('');
  }

  Future<File> writeJWT(String data) => storage.writeJWT(data);

  Map<DateTime, List<Map<String, dynamic>>> sortSurgeriesByDate(
      List<dynamic> surgeries) {
    Map<DateTime, List<Map<String, dynamic>>> surgeriesByDate =
        Map<DateTime, List<Map<String, dynamic>>>();
    Set<DateTime> datesOfSurgeries = Set<DateTime>();

    for (var surgery in surgeries) {
      final _date = surgery['date'].toString().split(" ")[0];
      final DateTime date = DateTime.parse(_date);
      datesOfSurgeries.add(date);
    }

    for (var date in datesOfSurgeries) surgeriesByDate[date] = [];

    surgeriesByDate.forEach((k, v) {
      for (var surgery in surgeries) {
        final date = DateTime.parse(surgery['date'].toString().split(" ")[0]);
        if (k.compareTo(date) == 0) surgeriesByDate[k].add(surgery);
      }
    });

    return surgeriesByDate;
  }

  List<Widget> appBarActions() => <Widget>[
        IconButton(
            icon: Icon(Icons.list, color: Colors.white),
            onPressed: handleSurgeriesListIconPressed),
        IconButton(
            icon: Icon(Icons.account_circle, color: Colors.white),
            onPressed: handleAccountIconPressed),
        Mutation(
            options: MutationOptions(documentNode: gql(Mutations.LOGOUT)),
            builder: (RunMutation runMutation, QueryResult res) {
              return IconButton(
                  icon: Icon(Icons.exit_to_app),
                  onPressed: () => handleLogoutIconPressed(runMutation));
            }),
      ];

  Query querySurgeries() {
    final String pendingSurgeries = isAdmin
        ? Queries.PENDING_SURGERIES_FOR_ADMIN
        : Queries.PENDING_SURGERIES_FOR_DOCTOR;

    return Query(
      options: QueryOptions(documentNode: gql(pendingSurgeries)),
      builder: (QueryResult res, {VoidCallback refetch, FetchMore fetchMore}) {
        if (res.hasException) return Text(res.exception.toString());
        if (res.loading) return Expanded(child: CircularProgressBar());

        Map<DateTime, List<Map<String, dynamic>>> surgeriesByDate =
            sortSurgeriesByDate(res.data["pendingSurgeries"]);

        return BuildCalendar(
            calendarCtrl: calendarCtrl, surgeriesByDate: surgeriesByDate);
      },
    );
  }

  Subscription updateSurgeries() => Subscription(
        'subPendingSurgeries',
        Subscriptions.SUB_PENDING_SURGERIES,
        builder: ({bool loading, dynamic payload, dynamic error}) {
          if (payload == null) {
            return querySurgeries();
          } else {
            Map<DateTime, List<Map<String, dynamic>>> surgeriesByDate =
                sortSurgeriesByDate(payload['subPendingSurgeries']);

            return BuildCalendar(
                calendarCtrl: calendarCtrl, surgeriesByDate: surgeriesByDate);
          }
        },
      );

  Query getJornada() => Query(
      options: QueryOptions(documentNode: gql(Queries.GET_SEASON)),
      builder: (QueryResult res, {VoidCallback refetch, FetchMore fetchMore}) {
        if (res.hasException) return Text(res.exception.toString());
        if (res.loading) return Text('');

        List _jornadaDays = res.data['getSeason'];
        Map<DateTime, List> _jornada = Map<DateTime, List>();
        Map<DateTime, List> jornada = Provider.of<States>(context).getJornada;
        for (var day in _jornadaDays) _jornada[DateTime.parse(day)] = [1];

        if (jornada.length == 0)
          SchedulerBinding.instance.addPostFrameCallback((_) {
            Provider.of<States>(context, listen: false).setJornada = _jornada;
          });
        return Text('', style: TextStyle(fontSize: 0.00));
      });

  Query queryAllSurgeries() => Query(
        options: QueryOptions(documentNode: gql(Queries.ALL_SURGERIES)),
        builder: (QueryResult res,
            {VoidCallback refetch, FetchMore fetchMore}) {
          if (res.hasException) return Text(res.exception.toString());
          if (res.loading) return Text('', style: TextStyle(fontSize: 0.00));
          Provider.of<States>(context, listen: false).setAllSurgeries =
              res.data['allSurgeries'];
          return Text('', style: TextStyle(fontSize: 0.00));
        },
      );

  Query queryDocData() => Query(
        options: QueryOptions(documentNode: gql(Queries.GET_DOCTORS)),
        builder: (QueryResult res,
            {VoidCallback refetch, FetchMore fetchMore}) {
          if (res.hasException) return Text(res.exception.toString());
          if (res.loading) return Text('', style: TextStyle(fontSize: 0.00));
          Provider.of<States>(context, listen: false).setAllDocData =
              res.data['getDoctors'];
          return Text('', style: TextStyle(fontSize: 0.00));
        },
      );

  Query queryMyInfo(String profesionalID) => Query(
        options: QueryOptions(
            documentNode: gql(Queries.DOCTOR),
            variables: {'profesionalID': profesionalID}),
        builder: (QueryResult res,
            {VoidCallback refetch, FetchMore fetchMore}) {
          if (res.hasException) return Text(res.exception.toString());
          if (res.loading) return Text('', style: TextStyle(fontSize: 1.00));
          Provider.of<States>(context, listen: false).setDoctorData =
              res.data['doctor'];
          return Text('', style: TextStyle(fontSize: 1.00));
        },
      );

  Widget build(BuildContext context) {
    final String token = Provider.of<States>(context).getJwtoken;
    final Map<String, dynamic> payload = parseJwt(token);

    return Scaffold(
        appBar: AppBar(
          title: Text('Programación de cirugías'),
          backgroundColor: Constants.MAIN_COLOR,
          actions: appBarActions(),
        ),
        body: Column(children: <Widget>[
          isAdmin ? updateSurgeries() : querySurgeries(),
          queryAllSurgeries(),
          getJornada(),
          isAdmin ? queryDocData() : queryMyInfo(payload['profesionalID']),
        ]));
  }
}
