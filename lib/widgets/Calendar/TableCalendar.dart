import 'package:flutter/material.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:provider/provider.dart';

import 'package:frontend/widgets/AvailableHoursList/AvailableHoursList.dart';

import 'package:frontend/provider/index.dart';

import 'package:frontend/constants.dart' as Constants;

class BuildCalendar extends StatelessWidget {
  final CalendarController calendarCtrl;
  final Map<DateTime, List<Map<String, dynamic>>> surgeriesByDate;

  BuildCalendar({Key key, this.calendarCtrl, this.surgeriesByDate})
      : super(key: key);

  void showMessage(BuildContext context, String message) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            title: Text(
              message,
              style: TextStyle(fontSize: 25.0),
            ),
            children: <Widget>[
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: RaisedButton(
                  onPressed: null,
                  disabledColor: Constants.MAIN_COLOR,
                  child: Text(
                    'Aceptar',
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.white, fontSize: 25.0),
                  ),
                ),
              ),
            ],
          );
        });
  }

  void availableHours(BuildContext context, DateTime date, List events) {
    final _date = date.subtract(Duration(hours: 7)).toLocal();
    final dayOfWeek = date.subtract(Duration(hours: 12)).weekday;

    if (dayOfWeek != 7) {
      Provider.of<States>(context, listen: false).setDaySelected = _date;
      Navigator.of(context)
          .push(MaterialPageRoute<void>(builder: (BuildContext context) {
        return Scaffold(body: AvailableHours(dayOfWeek: dayOfWeek));
      }));
    } else {
      showMessage(context, 'Día no disponible.');
    }
  }

  AnimatedContainer builderEventsMarker(
      DateTime time, List events, bool isJornada) {
    final Duration duration = const Duration(milliseconds: 300);
    final BoxDecoration decoration = BoxDecoration(
        shape: BoxShape.circle,
        color: isJornada ? Colors.red : const Color(0xFF114b5f));
    final String text = isJornada ? 'J' : '${events.length}';
    final TextStyle style = TextStyle(color: Colors.white, fontSize: 15.0);

    return AnimatedContainer(
      duration: duration,
      decoration: decoration,
      width: 20.0,
      height: 20.0,
      child: Center(child: Text(text, style: style)),
    );
  }

  CalendarBuilders calendarBuilders() =>
      CalendarBuilders(markersBuilder: (context, date, events, holidays) {
        final List<Widget> children = List<Widget>();

        if (events.isNotEmpty)
          children.add(Positioned(
            right: 1,
            bottom: 1,
            child: builderEventsMarker(date, events, false),
          ));
        if (holidays.isNotEmpty)
          children.add(Positioned(
              right: 1,
              bottom: 1,
              child: builderEventsMarker(date, holidays, true)));

        return children;
      });

  Widget build(BuildContext context) {
    initializeDateFormatting('es_MX', null).then((_) {});
    const Locale spanish = Locale('es_MX');
    final Map<DateTime, List<dynamic>> jornada =
        Provider.of<States>(context).getJornada;
    final HeaderStyle headerStyle = HeaderStyle(formatButtonVisible: false);
    final CalendarStyle calendarStyle = CalendarStyle(
        selectedColor: Constants.MAIN_COLOR,
        todayColor: const Color(0xFF44CCFF));

    return TableCalendar(
        calendarController: calendarCtrl,
        locale: spanish.toString(),
        headerStyle: headerStyle,
        calendarStyle: calendarStyle,
        onDaySelected: (date, events) => availableHours(context, date, events),
        events: surgeriesByDate,
        holidays: jornada,
        builders: calendarBuilders());
  }
}
