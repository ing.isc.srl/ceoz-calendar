import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ntp/ntp.dart';

import 'package:frontend/provider/index.dart';

import 'package:frontend/widgets/FormAddSurgery/FormAddSurgery.dart';

import 'package:frontend/constants.dart' as Constants;

class Tile extends StatelessWidget {
  final String hour;
  final String message;
  final String status;
  final Map<String, dynamic> hours;

  Tile({Key key, this.hour, this.message, this.status, this.hours})
      : super(key: key);

  void addNewSurgery(BuildContext context) {
    double availableHours = 0;
    for (var i = hours['initHours'].indexOf(hour);
        i <= hours['initHours'].length - 1;
        i++) {
      if (hours['ownBusyHours'].contains(hours['initHours'][i]) ||
          hours['busyHours'].contains(hours['initHours'][i]) ||
          hours['pendingHours'].contains(hours['initHours'][i])) break;
      availableHours += 0.5;
    }
    Navigator.of(context).push(MaterialPageRoute<void>(
        builder: (BuildContext context) =>
            Scaffold(body: FormAddSurgery(availableTime: availableHours))));
  }

  void onTap(BuildContext context) async {
    final DateTime currentDate = await NTP.now();
    const String message =
        'Solo pueden programarse cirugias en horario laboral';

    switch (currentDate.weekday) {
      case 6:
        {
          if (currentDate.hour < 8 || currentDate.hour >= 14) {
            showMessage(context, message);
          } else {
            addNewSurgery(context);
            Provider.of<States>(context, listen: false).setSelectedHour =
                '$hour:00';
          }
          break;
        }
      case 7:
        {
          showMessage(context, message);
          break;
        }
      default:
        {
          if (currentDate.hour < 8 || currentDate.hour >= 18) {
            showMessage(context, message);
          } else {
            addNewSurgery(context);
            Provider.of<States>(context, listen: false).setSelectedHour =
                '$hour:00';
          }
          break;
        }
    }
  }

  Future<bool> showMessage(BuildContext context, String message) async =>
      await showDialog(
          context: context,
          builder: (BuildContext context) {
            return SimpleDialog(
              title: Text(
                message,
                style: TextStyle(fontSize: 25.0),
              ),
              children: <Widget>[
                SimpleDialogOption(
                  onPressed: () {
                    Navigator.of(context).pop(true);
                  },
                  child: RaisedButton(
                    onPressed: null,
                    disabledColor: Constants.MAIN_COLOR,
                    child: Text(
                      'Aceptar',
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white, fontSize: 25.0),
                    ),
                  ),
                ),
              ],
            );
          });

  ListTile availableTile(BuildContext context) {
    final DateTime selectedDate =
        Provider.of<States>(context, listen: false).getDaySelected;
    final bool unselect =
        selectedDate.weekday == 6 ? hour == "14:30" : hour == "18:30";

    return ListTile(
      title: Text(
        hour,
        style: TextStyle(
          fontSize: 30.0,
        ),
      ),
      onTap: unselect ? null : () => onTap(context),
    );
  }

  Container busyTile() {
    final titleStyle = message == "OCUPADO"
        ? TextStyle(color: Colors.white, fontWeight: FontWeight.bold)
        : TextStyle(fontSize: 20.0, color: Colors.white);
    final tileColor = message == "OCUPADO"
        ? const Color(0x88FF0000)
        : status != "Pendiente"
            ? Colors.lightBlue[300]
            : const Color(0x88888888);

    return Container(
        decoration: BoxDecoration(color: tileColor),
        child: ListTile(
          leading: Text(hour,
              style: TextStyle(
                fontSize: 30.0,
                color: Colors.white,
              )),
          title: Text(
            message,
            style: titleStyle,
          ),
        ));
  }

  Widget build(BuildContext context) =>
      hours != null ? availableTile(context) : busyTile();
}
