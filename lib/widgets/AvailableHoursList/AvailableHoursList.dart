import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';

import 'package:frontend/widgets/CircularProgressBar/CircularProgressBar.dart';
import 'package:frontend/provider/index.dart';
import 'package:frontend/jwt/index.dart';
import 'Tile.dart';

import 'package:frontend/graphql/Subscriptions.dart' as Subscriptions;
import 'package:frontend/graphql/Mutations.dart' as Mutations;
import 'package:frontend/graphql/Queries.dart' as Queries;
import 'package:frontend/constants.dart' as Constants;

class AvailableHours extends StatefulWidget {
  final int dayOfWeek;
  AvailableHours({Key key, this.dayOfWeek}) : super(key: key);
  AvailableHoursState createState() => AvailableHoursState();
}

class AvailableHoursState extends State<AvailableHours> {
  List<String> hoursList = [];

  void initState() {
    super.initState();
    hoursList = widget.dayOfWeek == 6
        ? Constants.SATURDAY_HOURS
        : Constants.WEEKDAY_HOURS;
  }

  void dispose() {
    super.dispose();
  }

  void onCompleted(dynamic res) async {
    final bool _res = await showMessage('Jornada establecida.');
    if (_res ?? true) Navigator.pop(context);
  }

  void setJornada(
      DateTime initDay, DateTime secondDay, RunMutation runMutation) async {
    String firstDateText =
        '${initDay.day} de ${Constants.MONTHS[initDay.month - 1]} del ${initDay.year}';
    String secondDateText =
        '${secondDay.day} de ${Constants.MONTHS[secondDay.month - 1]} del ${secondDay.year}';
    String message =
        '¿Programar jornada del $firstDateText al $secondDateText?';

    final bool confirm = await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Establecer jornada"),
          content: Text(
            message,
            style: TextStyle(fontSize: 20.0),
          ),
          actions: <Widget>[
            FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: const Text("CANCELAR")),
            FlatButton(
              onPressed: () => Navigator.of(context).pop(true),
              child: Text("CONFIRMAR"),
            ),
          ],
        );
      },
    );

    if (confirm ?? false)
      runMutation({
        'firstDate': initDay.toString().substring(0, 19),
        'SecondDate': initDay.add(Duration(days: 1)).toString().substring(0, 19)
      });
  }

  Future<bool> showMessage(String message) async => await showDialog(
      context: context,
      builder: (BuildContext context) => SimpleDialog(
            title: Text(
              message,
              style: TextStyle(fontSize: 25.0),
            ),
            children: <Widget>[
              SimpleDialogOption(
                onPressed: () => Navigator.of(context).pop(true),
                child: RaisedButton(
                  onPressed: null,
                  disabledColor: Constants.MAIN_COLOR,
                  child: Text(
                    'Aceptar',
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.white, fontSize: 25.0),
                  ),
                ),
              ),
            ],
          ));

  Map<String, dynamic> sortData(List<dynamic> surgeries) {
    final String token = Provider.of<States>(context).getJwtoken;
    final Map<String, dynamic> decodedToken = parseJwt(token);
    Map<String, String> ownInitHourSurgery = Map<String, String>();
    Map<String, String> pendingHourSurgery = Map<String, String>();
    List<String> pendingHours = [];
    List<String> busyHours = [];
    List<String> ownBusyHours = [];

    for (var surgery in surgeries) {
      final String busyHour = surgery['date'].toString().substring(11, 16);
      final int firstIndex = hoursList.indexOf(busyHour);
      final int lastIndex = firstIndex + (surgery['duration'] * 2.0).toInt();
      final Iterator<String> notAvailableHours =
          hoursList.getRange(firstIndex, lastIndex).iterator;

      if (decodedToken['rol'] != 0 &&
          decodedToken['profesionalID'] != surgery['profesionalID']) {
        while (notAvailableHours.moveNext())
          busyHours.add(notAvailableHours.current);
        continue;
      }

      switch (surgery['status']) {
        case "Pendiente":
          {
            pendingHourSurgery[busyHour] = '${surgery['name']} - Pendiente';
            try {
              while (notAvailableHours.moveNext())
                pendingHours.add(notAvailableHours.current);
            } catch (e) {}
            break;
          }
        default:
          {
            ownInitHourSurgery[busyHour] =
                '${surgery['name']} - ${surgery['patientName']}';
            try {
              while (notAvailableHours.moveNext())
                ownBusyHours.add(notAvailableHours.current);
            } catch (e) {}
          }
      }
    }
    return {
      'ownInitHourSurgery': ownInitHourSurgery,
      'pendingHourSurgery': pendingHourSurgery,
      'pendingHours': pendingHours,
      'busyHours': busyHours,
      'ownBusyHours': ownBusyHours
    };
  }

  Padding titleHeader() => Padding(
        padding: const EdgeInsets.all(10.0),
        child: Text(
          'Selecciona una hora disponible para continuar',
          style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
        ),
      );

  Query queryAvailableHours() {
    final DateTime selectedDay = Provider.of<States>(context).getDaySelected;
    final String month = selectedDay.month < 10
        ? "0${selectedDay.month}"
        : '${selectedDay.month}';
    final String day =
        selectedDay.day < 10 ? "0${selectedDay.day}" : '${selectedDay.day}';

    return Query(
        options: QueryOptions(
            documentNode: gql(Queries.SURGERIES_BY_DATE),
            variables: {'date': "${selectedDay.year}-$month-$day"}),
        builder: (QueryResult res,
            {VoidCallback refetch, FetchMore fetchMore}) {
          if (res.hasException) return Text(res.exception.toString());
          if (res.loading) return Expanded(child: CircularProgressBar());

          final Map<String, dynamic> sortedData =
              sortData(res.data['surgeriesByDate']);

          return Expanded(
              child: ListView(
                  children: _initHours(tiles(
                      initHours: hoursList,
                      busyHours: sortedData['busyHours'],
                      ownBusyHours: sortedData['ownBusyHours'],
                      ownInitHourSurgery: sortedData['ownInitHourSurgery'],
                      pendingHours: sortedData['pendingHours'],
                      pendingHourSurgery: sortedData['pendingHourSurgery']))));
        });
  }

  Subscription updateAvailableHours() =>
      Subscription('subSurgeries', Subscriptions.SUB_SURGERIES,
          builder: ({bool loading, dynamic payload, dynamic error}) {
        if (payload == null) {
          return queryAvailableHours();
        } else {
          final Map<String, dynamic> sortedData =
              sortData(payload['subSurgeries']);

          return Expanded(
              child: ListView(
                  children: _initHours(tiles(
                      initHours: hoursList,
                      busyHours: sortedData['busyHours'],
                      ownBusyHours: sortedData['ownBusyHours'],
                      ownInitHourSurgery: sortedData['ownInitHourSurgery'],
                      pendingHours: sortedData['pendingHours'],
                      pendingHourSurgery: sortedData['pendingHourSurgery']))));
        }
      });

  Iterable<Padding> tiles(
      {List<String> initHours,
      List<String> busyHours,
      List<String> ownBusyHours,
      Map<String, String> ownInitHourSurgery,
      List<String> pendingHours,
      Map<String, String> pendingHourSurgery}) {
    final DateTime initDay = Provider.of<States>(context).getDaySelected;
    final Map<DateTime, List> jornada = Provider.of<States>(context).getJornada;
    final List jornadaDays = jornada.keys.toList();
    final EdgeInsets paddingTile = const EdgeInsets.symmetric(vertical: 0.0);
    Iterable<Padding> tiles;

    if (jornadaDays.contains(initDay)) {
      tiles = initHours.map((String hour) {
        Widget child = Tile(hour: hour, message: 'OCUPADO');
        return Padding(padding: paddingTile, child: child);
      });
    } else {
      tiles = initHours.map((String hour) {
        Widget child;
        if (busyHours.contains(hour)) {
          child = Tile(hour: hour, message: 'OCUPADO');
        } else if (ownBusyHours.contains(hour)) {
          child = Tile(hour: hour, message: ownInitHourSurgery[hour] ?? '');
        } else if (pendingHours.contains(hour)) {
          child = Tile(
              hour: hour,
              message: pendingHourSurgery[hour] ?? '',
              status: "Pendiente");
        } else {
          child = Tile(
            hour: hour,
            hours: {
              'initHours': initHours,
              'busyHours': busyHours,
              'ownBusyHours': ownBusyHours,
              'pendingHours': pendingHours
            },
          );
        }

        return Padding(padding: paddingTile, child: child);
      });
    }

    return tiles;
  }

  List<Widget> _initHours(Iterable<Padding> tiles) =>
      ListTile.divideTiles(context: context, tiles: tiles).toList();

  Mutation setJornadaButton() => Mutation(
      options: MutationOptions(
          documentNode: gql(Mutations.SAVE_SEASON), onCompleted: onCompleted),
      builder: (RunMutation runMutation, QueryResult queryResult) {
        final DateTime initDay = Provider.of<States>(context).getDaySelected;
        final DateTime secondDay = initDay.add(Duration(days: 1));

        return Padding(
          padding: const EdgeInsets.symmetric(vertical: 5),
          child: RaisedButton(
            onPressed: () => setJornada(initDay, secondDay, runMutation),
            child: Text(
              'Establecer jornada',
              style: TextStyle(fontSize: 25.0),
            ),
            textColor: Colors.white,
            color: Constants.MAIN_COLOR,
          ),
        );
      });

  Widget build(BuildContext context) {
    final bool isAdmin = Provider.of<States>(context, listen: false).getIsAdmin;
    final Map<DateTime, List> jornada =
        Provider.of<States>(context, listen: false).getJornada;

    return Scaffold(
        appBar: AppBar(
          title: Text('Programación de cirugías'),
          backgroundColor: Constants.MAIN_COLOR,
        ),
        body: Column(
          children: <Widget>[
            titleHeader(),
            updateAvailableHours(),
            isAdmin && jornada.keys.length == 0 ? setJornadaButton() : null
          ].where((Object o) => o != null).toList(),
        ));
  }
}
