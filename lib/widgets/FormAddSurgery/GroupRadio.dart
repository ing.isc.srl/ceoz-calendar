import 'package:flutter/material.dart';

import 'package:frontend/constants.dart' as Constants;

class GroupRadio extends StatefulWidget {
  final Map<String, String> texts;
  final Map<String, dynamic> values;
  final Function onChange;
  final bool thereIsAvailableTime;

  GroupRadio(
      {Key key,
      this.texts,
      this.values,
      this.onChange,
      this.thereIsAvailableTime})
      : super(key: key);
  GroupRadioState createState() => GroupRadioState();
}

class GroupRadioState extends State<GroupRadio> {
  final TextStyle style = const TextStyle(fontSize: 20.0);

  Flexible question(String question) => Flexible(
      child: Text(question, textAlign: TextAlign.center, style: style));

  Radio radio(dynamic value) => Radio(
        value: value,
        activeColor: Constants.MAIN_COLOR,
        groupValue: widget.values['groupValue'],
        onChanged: (dynamic value) => widget.onChange(value),
      );

  Widget build(BuildContext context) {
    final bool render = widget.thereIsAvailableTime ?? true;

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        question(widget.texts['question']),
        radio(widget.values['valueOne']),
        Text(widget.texts['optionOne'], style: style),
        render ? radio(widget.values['valueTwo']) : null,
        render ? Text(widget.texts['optionTwo'], style: style) : null,
      ].where((Object o) => o != null).toList(),
    );
  }
}
