import 'package:flutter/material.dart';

import 'package:frontend/constants.dart' as Constants;

class StepperForm extends StatefulWidget {
  final List<Map<String, dynamic>> stepsContent;
  final GlobalKey<FormState> formKeyStepOne;
  final Widget mutation;

  StepperForm({Key key, this.stepsContent, this.formKeyStepOne, this.mutation})
      : super(key: key);

  StepperState createState() => StepperState();
}

class StepperState extends State<StepperForm> {
  int currentStep = 0;

  void onStepCancel() {
    if (currentStep > 0)
      setState(() => currentStep -= 1);
    else
      Navigator.of(context).pop();
  }

  void onStepContinue() {
    if (widget.formKeyStepOne.currentState.validate() && currentStep < 1)
      setState(() => currentStep += 1);
  }

  Step step(String text, GlobalKey<FormState> formKey, Widget child) => Step(
      title: Text(text),
      isActive: true,
      content: Form(key: formKey, child: child));

  Padding controlsBuilder(BuildContext context,
      {VoidCallback onStepContinue, VoidCallback onStepCancel}) {
    final String textBackButton = currentStep == 1 ? 'Atrás' : 'Cancelar';
    final TextStyle style = TextStyle(fontSize: 20.0);
    final Widget actionButton = currentStep == 1
        ? widget.mutation
        : FlatButton(
            onPressed: onStepContinue,
            child: Text('Continuar', style: style),
            textColor: Colors.white,
            color: Constants.MAIN_COLOR,
          );
    final FlatButton backButton = FlatButton(
        onPressed: onStepCancel, child: Text(textBackButton, style: style));

    return Padding(
        padding: const EdgeInsets.only(top: 20),
        child: Row(
          children: <Widget>[actionButton, backButton],
        ));
  }

  Widget build(BuildContext context) {
    final List<Step> steps = widget.stepsContent
        .map((stepContent) => step(
            stepContent['text'], stepContent['formKey'], stepContent['child']))
        .toList();

    return Stepper(
      steps: steps,
      currentStep: currentStep,
      controlsBuilder: controlsBuilder,
      onStepCancel: onStepCancel,
      onStepContinue: onStepContinue,
    );
  }
}
