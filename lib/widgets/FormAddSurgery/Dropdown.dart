import 'package:flutter/material.dart';

import 'package:frontend/constants.dart' as Constants;

class Dropdown extends StatelessWidget {
  final String value;
  final Function onChange;
  final List<String> items;

  Dropdown({Key key, this.value, this.onChange, this.items}) : super(key: key);

  List<DropdownMenuItem<String>> menuItems() => items
      .map<DropdownMenuItem<String>>((dynamic item) =>
          DropdownMenuItem<String>(value: item, child: Text(item)))
      .toList();

  Widget build(BuildContext context) {
    final Icon icon = Icon(Icons.arrow_drop_down);
    final TextStyle style =
        TextStyle(fontSize: 20.0, color: Constants.MAIN_COLOR);

    return DropdownButton(
      value: value,
      isExpanded: true,
      icon: icon,
      iconSize: 24,
      iconEnabledColor: Constants.MAIN_COLOR,
      elevation: 16,
      style: style,
      underline: Container(
        height: 2,
        color: const Color(0xFF44CCFF),
      ),
      onChanged: (String value) => onChange(value),
      items: menuItems(),
    );
  }
}
