import 'package:flutter/material.dart';

import 'package:frontend/constants.dart' as Constants;

class TextInput extends StatelessWidget {
  final TextEditingController controller;
  final String hintText;
  final String isEmptyText;

  TextInput({Key key, this.hintText, this.controller, this.isEmptyText})
      : super(key: key);

  final Color color = Constants.MAIN_COLOR;

  InputDecoration inputDecoration(String text) {
    final UnderlineInputBorder underline =
        UnderlineInputBorder(borderSide: BorderSide(color: color));
    final TextStyle textStyle = TextStyle(color: color);

    return InputDecoration(
      enabledBorder: underline,
      errorBorder: underline,
      errorStyle: textStyle,
      focusedErrorBorder: underline,
      labelText: text,
      labelStyle: textStyle,
      hintText: text,
      hintStyle: textStyle,
    );
  }

  String inputValidator(String value, String isEmptyText) =>
      value.isEmpty ? isEmptyText : null;

  Widget build(BuildContext context) {
    final TextInputType keyboardType =
        hintText == "Precio" ? TextInputType.number : TextInputType.text;
    final TextCapitalization capitalization = TextCapitalization.words;

    final TextStyle style =
        TextStyle(fontSize: 20.0, color: color, decorationColor: color);

    return TextFormField(
      keyboardType: keyboardType,
      controller: controller,
      textCapitalization: capitalization,
      decoration: inputDecoration(hintText),
      validator: (String value) => inputValidator(value, isEmptyText),
      style: style,
    );
  }
}
