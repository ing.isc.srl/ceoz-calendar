import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:frontend/provider/index.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import 'dart:async';

import 'Stepper.dart';
import 'Dropdown.dart';
import 'TextInput.dart';
import 'GroupRadio.dart';

import 'package:frontend/graphql/Mutations.dart' as Mutations;
import 'package:frontend/constants.dart' as Constants;

enum Anesthesia { local, general }
enum IntraOcularLen { yes, no }
enum FlexibleOrPMMA { flexible, pmma }
enum Silicon { yes, no }
enum NeedInputs { yes, no }

class FormAddSurgery extends StatefulWidget {
  final double availableTime;
  FormAddSurgery({Key key, this.availableTime}) : super(key: key);
  FormAddSurgeryState createState() => FormAddSurgeryState();
}

class FormAddSurgeryState extends State<FormAddSurgery> {
  final TextEditingController dioptriaFieldController = TextEditingController();
  final TextEditingController otherInputController = TextEditingController();
  final TextEditingController patientNameController = TextEditingController();
  final TextEditingController quantityFieldController = TextEditingController();
  final GlobalKey<FormState> formKeyStepOne = GlobalKey<FormState>();
  final GlobalKey<FormState> formKeyStepTwo = GlobalKey<FormState>();

  FlexibleOrPMMA flexibleOrPMMA = FlexibleOrPMMA.flexible;
  IntraOcularLen intraOcularLen = IntraOcularLen.no;
  Anesthesia anesthesia = Anesthesia.local;
  NeedInputs needInputs = NeedInputs.no;
  Silicon silicon = Silicon.no;
  String doctorsName = "Doctor...";
  String surgeryName = "Cirugía...";
  double surgeryDuration = 0;
  int currentStep = 0;

  void dispose() {
    dioptriaFieldController.dispose();
    otherInputController.dispose();
    patientNameController.dispose();
    quantityFieldController.dispose();
    super.dispose();
  }

  void showSnackbar() {
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text(
        '1 hora más por ser anestesia general',
        style: TextStyle(fontSize: 20.0),
        textAlign: TextAlign.center,
      ),
      duration: Duration(seconds: 5),
      backgroundColor: Constants.MAIN_COLOR,
    ));
  }

  Future<bool> showMessage(String message) async => await showDialog(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: Text(
            message,
            style: TextStyle(fontSize: 20.0),
          ),
          children: <Widget>[
            SimpleDialogOption(
                onPressed: () => Navigator.of(context).pop(true),
                child: RaisedButton(
                  onPressed: null,
                  disabledColor: Constants.MAIN_COLOR,
                  child: Text(
                    'Aceptar',
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.white, fontSize: 25.0),
                  ),
                )),
          ],
        );
      });

  List<String> getDoctorsList() {
    final List<dynamic> allDoctors =
        Provider.of<States>(context, listen: false).getAllDocData;
    final List<String> doctors = List<String>();
    allDoctors.forEach((dynamic doctor) => doctors.add(doctor['name']));

    doctors.sort();
    doctors.insert(0, 'Doctor...');

    return doctors;
  }

  List<String> getSurgeriesList() {
    final List<dynamic> allSurgeries =
        Provider.of<States>(context, listen: false).getAllSurgeries;
    final List<String> surgeries = List<String>();

    allSurgeries.forEach((dynamic surgery) {
      final double duration = surgery['duration'].toDouble();
      if (duration <= widget.availableTime) surgeries.add(surgery['name']);
    });

    surgeries.sort();
    surgeries.insert(0, 'Cirugía...');

    return surgeries;
  }

  Map<String, double> getDurationOfEachSurgery() {
    final List<dynamic> allSurgeries =
        Provider.of<States>(context, listen: false).getAllSurgeries;
    Map<String, double> durationOfEachSurgery = Map<String, double>();

    allSurgeries.forEach((dynamic surgery) {
      final double duration = surgery['duration'].toDouble();
      if (duration <= widget.availableTime)
        durationOfEachSurgery[surgery['name']] = duration;
    });

    return durationOfEachSurgery;
  }

  Map<String, String> getIdOfEachDoctor() {
    final List<dynamic> allDoctors =
        Provider.of<States>(context, listen: false).getAllDocData;
    Map<String, String> doctorID = Map<String, String>();

    allDoctors.forEach(
        (dynamic doctor) => doctorID[doctor['name']] = doctor['profesionalID']);

    return doctorID;
  }

  void onCompleted(dynamic res) async {
    final bool isAdmin = Provider.of<States>(context, listen: false).getIsAdmin;
    final String operationName = isAdmin ? "newSurgeryByAdmin" : "newSurgery";
    final String successAdminMessage = 'Nueva cirugía programada.';
    final String successDoctorMessage =
        'Nueva cirugía programada, debes llamar a la dirección de CEOZ antes de las 07:00 PM para confirmar el registro, de lo contrario será cancelada. La programación es válida hasta que la dirección está enterada vía telefónica.';

    if (res[operationName] == 'Surgery added') {
      bool res = await showMessage(
          !isAdmin ? successDoctorMessage : successAdminMessage);

      if (res ?? true) Navigator.of(context).pop();
    }
  }

  void onPressed(RunMutation runMutation) {
    final DateTime date =
        Provider.of<States>(context, listen: false).getDaySelected;
    final String hour =
        Provider.of<States>(context, listen: false).getSelectedHour;
    final String surgeryDate = '${date.toString().substring(0, 10)} $hour';
    final bool isAdmin = Provider.of<States>(context, listen: false).getIsAdmin;
    final Map<String, String> doctorID = getIdOfEachDoctor();

    final bool intraOcular = intraOcularLen == IntraOcularLen.yes;
    final bool needSilicon = silicon == Silicon.yes;
    final String typeOfAnesthesia =
        anesthesia == Anesthesia.local ? "Local" : "General";
    final String _flexibleOrPMMA =
        flexibleOrPMMA == FlexibleOrPMMA.flexible ? "Flexible" : "PMMA";
    final String otherInput = otherInputController.text.length > 0
        ? otherInputController.text
        : "Ninguno";

    Map<String, dynamic> data = {
      'patientName': patientNameController.text,
      'name': surgeryName,
      'date': surgeryDate,
      'anesthesia': typeOfAnesthesia,
      'price': quantityFieldController.text,
      'needLens': intraOcular,
      'dioptria': dioptriaFieldController.text,
      'flexibleOrPMMA': _flexibleOrPMMA,
      'needSilicon': needSilicon,
      'otherThing': otherInput
    };

    if (isAdmin) data.putIfAbsent('profesionalID', () => doctorID[doctorsName]);

    if (surgeryName != 'Cirugía...' && formKeyStepTwo.currentState.validate()) {
      runMutation(data);
    } else {
      showMessage('Seleccione una cirugía');
    }
  }

  Column formSurgery() {
    final bool isAdmin = Provider.of<States>(context, listen: false).getIsAdmin;

    final Map<String, double> durationOfEachSurgery =
        getDurationOfEachSurgery();
    final List<String> surgeries = getSurgeriesList();
    final List<String> doctors = getDoctorsList();

    List<Widget> children = [
      Dropdown(
          value: surgeryName,
          items: surgeries,
          onChange: (String value) {
            if (value == "Cirugía...") {
              showMessage('Seleccione una cirugía');
            } else {
              setState(() {
                surgeryName = value;
                surgeryDuration = durationOfEachSurgery[value];
              });
            }
          }),
      TextInput(
          controller: patientNameController,
          hintText: "Nombre del paciente",
          isEmptyText: "Ingrese nombre del paciente:"),
      TextInput(
          controller: quantityFieldController,
          hintText: "Precio",
          isEmptyText: "Ingrese precio:"),
      GroupRadio(
          texts: {
            'question': "Anestesia: ",
            'optionOne': "Local",
            'optionTwo': "General"
          },
          values: {
            'valueOne': Anesthesia.local,
            'valueTwo': Anesthesia.general,
            'groupValue': anesthesia,
          },
          onChange: (value) {
            if (value == Anesthesia.general) showSnackbar();
            setState(() => anesthesia = value);
          },
          thereIsAvailableTime:
              (widget.availableTime - surgeryDuration).toInt() > 0),
    ];

    if (isAdmin)
      children.insert(
          0,
          Dropdown(
              value: doctorsName,
              items: doctors,
              onChange: (String value) {
                if (value == "Doctor...") {
                  showMessage('Seleccione un doctor');
                } else {
                  setState(() {
                    doctorsName = value;
                  });
                }
              }));

    return Column(children: children);
  }

  Column questions() => Column(
          children: [
        TextInput(
            controller: dioptriaFieldController,
            hintText: "¿Que dioptria es tu lente?",
            isEmptyText: "¿Que dioptria es tu lente?"),
        GroupRadio(
          texts: {
            'question': "¿Necesitas lente intraocular?",
            'optionOne': "Si",
            'optionTwo': "No"
          },
          values: {
            'valueOne': IntraOcularLen.yes,
            'valueTwo': IntraOcularLen.no,
            'groupValue': intraOcularLen,
          },
          onChange: (value) => setState(() => intraOcularLen = value),
        ),
        GroupRadio(
          texts: {
            'question': "¿Es flexible o PMMA?",
            'optionOne': "PMMA",
            'optionTwo': "Flexible"
          },
          values: {
            'valueOne': FlexibleOrPMMA.pmma,
            'valueTwo': FlexibleOrPMMA.flexible,
            'groupValue': flexibleOrPMMA,
          },
          onChange: (value) => setState(() => flexibleOrPMMA = value),
        ),
        GroupRadio(
          texts: {
            'question': "¿Necesitas silicon?",
            'optionOne': "Si",
            'optionTwo': "No",
          },
          values: {
            'valueOne': Silicon.yes,
            'valueTwo': Silicon.no,
            'groupValue': silicon,
          },
          onChange: (value) => setState(() => silicon = value),
        ),
        GroupRadio(
          texts: {
            'question': "¿Necesitas otro insumo?",
            'optionOne': "Si",
            'optionTwo': "No",
          },
          values: {
            'valueOne': NeedInputs.yes,
            'valueTwo': NeedInputs.no,
            'groupValue': needInputs,
          },
          onChange: (value) => setState(() => needInputs = value),
        ),
        needInputs == NeedInputs.yes
            ? TextInput(
                controller: otherInputController,
                hintText: "¿Que otro insumo necesitas?",
                isEmptyText: "¿Que otro insumo necesitas?")
            : null
      ].where((Object o) => o != null).toList());

  Mutation mutation() {
    final bool isAdmin = Provider.of<States>(context).getIsAdmin;
    final String operation =
        isAdmin ? Mutations.NEW_SURGERY_BY_ADMIN : Mutations.NEW_SURGERY;

    return Mutation(
        options: MutationOptions(
            documentNode: gql(operation), onCompleted: onCompleted),
        builder: (RunMutation runMutation, QueryResult result) {
          return RaisedButton(
            onPressed: () => onPressed(runMutation),
            child: Text(
              'Confirmar',
              style: TextStyle(fontSize: 20.0),
            ),
            textColor: Colors.white,
            color: Constants.MAIN_COLOR,
          );
        });
  }

  LayoutBuilder layoutBuilder(Widget child) => LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constrains) =>
          SingleChildScrollView(
              child: ConstrainedBox(
                  constraints: BoxConstraints(minHeight: constrains.maxHeight),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[child]))));

  Widget build(BuildContext context) {
    final List<Map<String, dynamic>> stepsContent = [
      {
        'text': 'Nueva cirugía',
        'formKey': formKeyStepOne,
        'child': formSurgery()
      },
      {'text': '', 'formKey': formKeyStepTwo, 'child': questions()}
    ];

    return Scaffold(
        appBar: AppBar(
          title: Text('Programar cirugía'),
          backgroundColor: Constants.MAIN_COLOR,
        ),
        body: layoutBuilder(StepperForm(
            stepsContent: stepsContent,
            formKeyStepOne: formKeyStepOne,
            mutation: mutation())));
  }
}
