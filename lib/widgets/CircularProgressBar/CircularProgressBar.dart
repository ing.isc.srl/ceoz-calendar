import 'package:flutter/material.dart';

import 'package:frontend/constants.dart' as Constants;

class CircularProgressBar extends StatelessWidget {
  Widget build(BuildContext context) => Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Center(
              child: CircularProgressIndicator(
            backgroundColor: Constants.MAIN_COLOR,
            strokeWidth: 12.0,
          ))
        ],
      );
}
