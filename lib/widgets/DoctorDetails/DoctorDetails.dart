import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:frontend/widgets/ModalChangePassword/ModalChangePass.dart';

import 'package:frontend/provider/index.dart';
import 'package:frontend/jwt/index.dart';

import 'package:frontend/constants.dart' as Constants;

class DoctorDetail extends StatefulWidget {
  DoctorDetailState createState() => DoctorDetailState();
}

class DoctorDetailState extends State<DoctorDetail> {
  void onPressed() =>
      showDialog(context: context, builder: (_) => ModalChangePass());

  List<Widget> sortData(Map<String, dynamic> doctorData) {
    List<Widget> info = List<Widget>();
    Map<String, dynamic> sortedData = {
      'Nombre: ': doctorData['name'],
      'Cédula profesional: ': doctorData['profesionalID'],
      'Especialidad: ': doctorData['specialty'],
      'Sub especialidad: ': doctorData['sub_specialty']
    };

    sortedData.forEach((key, value) => info.add(row(key, value)));

    return info;
  }

  Widget row(String leading, String content) {
    final EdgeInsets padding =
        EdgeInsets.symmetric(horizontal: 10.0, vertical: 25.0);
    final TextStyle leadingStyle =
        TextStyle(fontSize: 20.0, color: Constants.MAIN_COLOR);
    final TextStyle contentStyle = TextStyle(fontSize: 20.0);

    return Padding(
        padding: padding,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(leading, style: leadingStyle),
            Flexible(child: Text(content, style: contentStyle))
          ],
        ));
  }

  RaisedButton changePass() => RaisedButton(
      child: Text(
        'Cambiar contraseña',
        style: TextStyle(fontSize: 20.0),
      ),
      textColor: Colors.white,
      color: Constants.MAIN_COLOR,
      onPressed: onPressed);

  Widget queryDoctor(String profesionalID) {
    final Map<String, dynamic> doctorData =
        Provider.of<States>(context).getDoctorData;
    final List<Widget> children = sortData(doctorData);

    return Padding(
      padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 10.0),
      child: Center(
          child: Column(children: [Wrap(children: children, spacing: 8.0)])),
    );
  }

  SingleChildScrollView singleChildScrollView(List<Widget> children) =>
      SingleChildScrollView(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: children));

  Widget build(BuildContext context) {
    final String token = Provider.of<States>(context).getJwtoken;
    final Map<String, dynamic> payload = parseJwt(token);

    return Scaffold(
      appBar: AppBar(
        title: Text('Perfil'),
        backgroundColor: Constants.MAIN_COLOR,
      ),
      body: singleChildScrollView(
          [queryDoctor(payload['profesionalID']), changePass()]),
    );
  }
}
