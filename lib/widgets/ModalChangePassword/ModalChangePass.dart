import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'TextInput.dart';

import 'package:frontend/graphql/Mutations.dart' as Mutations;
import 'package:frontend/constants.dart' as Constants;

class ModalChangePass extends StatefulWidget {
  ModalChangePassState createState() => ModalChangePassState();
}

class ModalChangePassState extends State<ModalChangePass> {
  final Map<String, TextEditingController> controllers = {
    'oldPassword': TextEditingController(),
    'newPassword': TextEditingController(),
    'confirm': TextEditingController()
  };
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  bool match = true;

  void dispose() {
    controllers['oldPassword'].dispose();
    controllers['newPassword'].dispose();
    controllers['confirm'].dispose();
    super.dispose();
  }

  void showMessage(String message) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            title: Text(
              message,
              style: TextStyle(fontSize: 25.0),
            ),
            children: <Widget>[
              SimpleDialogOption(
                onPressed: () => Navigator.pop(context),
                child: RaisedButton(
                  onPressed: null,
                  disabledColor: Constants.MAIN_COLOR,
                  child: Text(
                    'Aceptar',
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.white, fontSize: 25.0),
                  ),
                ),
              ),
            ],
          );
        });
  }

  void onCompleted(dynamic res) {
    switch (res['changePassword']) {
      case 'Contraseña actual incorrecta':
        {
          showMessage(res['changePassword']);
          break;
        }
      case 'La nueva contraseña y la actual no pueden ser iguales':
        {
          showMessage(res['changePassword']);
          break;
        }
      default:
        {
          Navigator.of(context).pop();
          showMessage('Contraseña actualizada');
          break;
        }
    }
  }

  void onPressed(RunMutation runMutation) {
    if (formKey.currentState.validate())
      runMutation({
        'oldPass': controllers['oldPassword'].text,
        'newPass': controllers['newPassword'].text
      });
  }

  FlatButton cancel() => FlatButton(
        child: Text('Cancelar', style: TextStyle(fontSize: 20.0)),
        onPressed: () => Navigator.of(context).pop(),
      );

  Mutation changePassword() => Mutation(
      options: MutationOptions(
          documentNode: gql(Mutations.CHANGE_PASSWORD),
          onCompleted: onCompleted),
      builder: (RunMutation runMutation, QueryResult queryResult) {
        return RaisedButton(
            child: Text(
              'Confirmar',
              style: TextStyle(fontSize: 20.0),
            ),
            textColor: Colors.white,
            color: Constants.MAIN_COLOR,
            onPressed: () => onPressed(runMutation));
      });

  Widget build(BuildContext context) => AlertDialog(
        title: Text(
          'Cambiar contraseña',
          style: TextStyle(fontSize: 25.0),
        ),
        content: SingleChildScrollView(
          child: Form(
              key: formKey,
              child: ListBody(
                children: <Widget>[
                  TextInput(
                      text: "Antigua", controller: controllers['oldPassword']),
                  TextInput(
                    text: "Nueva",
                    controller: controllers['newPassword'],
                    match: match,
                    onChanged: (String value) => setState(() {
                      match = value == controllers['confirm'].text;
                    }),
                  ),
                  TextInput(
                    text: "Confirmar",
                    controller: controllers['confirm'],
                    match: match,
                    onChanged: (String value) => setState(() {
                      match = value == controllers['newPassword'].text;
                    }),
                  ),
                ],
              )),
        ),
        actions: <Widget>[cancel(), changePassword()],
      );
}
