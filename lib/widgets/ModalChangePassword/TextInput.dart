import 'package:flutter/material.dart';

import 'package:frontend/constants.dart' as Constants;

class TextInput extends StatefulWidget {
  final String text;
  final TextEditingController controller;
  final bool match;
  final Function onChanged;

  TextInput({Key key, this.text, this.controller, this.match, this.onChanged})
      : super(key: key);

  TextInputState createState() => TextInputState();
}

class TextInputState extends State<TextInput> {
  bool visible = false;

  InputDecoration decoration() {
    final UnderlineInputBorder underline = UnderlineInputBorder(
        borderSide: BorderSide(color: Constants.MAIN_COLOR));
    final UnderlineInputBorder errorUnderline =
        UnderlineInputBorder(borderSide: BorderSide(color: Colors.red));
    final Icon icon = Icon(visible ? Icons.visibility : Icons.visibility_off,
        color: Constants.MAIN_COLOR);
    final TextStyle style = TextStyle(color: Constants.MAIN_COLOR);

    return InputDecoration(
        enabledBorder: underline,
        errorBorder: errorUnderline,
        errorStyle: style,
        focusedErrorBorder: errorUnderline,
        labelText: '${widget.text} contraseña:',
        labelStyle: style,
        hintText: '${widget.text} contraseña',
        hintStyle: style,
        suffixIcon: IconButton(
          icon: icon,
          onPressed: () => setState(() => visible = !visible),
        ));
  }

  String validator(String value) {
    if (value.isEmpty) {
      return 'Ingrese contraseña';
    } else if (widget.text == 'Nueva' || widget.text == 'Confirmar') {
      if (!widget.match) return 'Las contraseñas no coinciden';
      if (widget.controller.text.length < 8) return "Mínimo 8 carácteres";
    }
    return null;
  }

  Widget build(BuildContext context) {
    final TextStyle style = TextStyle(
        fontSize: 18.0,
        color: Constants.MAIN_COLOR,
        decorationColor: Constants.MAIN_COLOR);

    return TextFormField(
      textInputAction: TextInputAction.done,
      obscureText: !visible,
      controller: widget.controller,
      decoration: decoration(),
      style: style,
      validator: validator,
      onChanged: widget?.onChanged,
    );
  }
}
