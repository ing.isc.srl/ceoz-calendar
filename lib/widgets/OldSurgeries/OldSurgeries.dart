import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:frontend/widgets/SurgeriesList/SurgeriesList.dart';

import 'package:provider/provider.dart';
import 'package:frontend/provider/index.dart';

import 'package:frontend/constants.dart' as Constants;

class OldSurgeries extends StatefulWidget {
  final List<Map<String, dynamic>> oldSurgeries;
  OldSurgeries({Key key, this.oldSurgeries}) : super(key: key);
  OldSurgeriesState createState() => OldSurgeriesState();
}

class OldSurgeriesState extends State<OldSurgeries> {
  Map<DateTime, List<Map<String, dynamic>>> sortOldSurgeriesByDate(
      int year, int month) {
    final bool isAdmin = Provider.of<States>(context, listen: false).getIsAdmin;
    Map<DateTime, List<Map<String, dynamic>>> oldSurgeriesByDate =
        Map<DateTime, List<Map<String, dynamic>>>();

    widget.oldSurgeries.forEach((oldSurgery) {
      if (oldSurgery['date'].month == month + 1 &&
          oldSurgery['date'].year == year) {
        final DateFormat dateFormat = DateFormat('yyyy-MM-dd 00:00:00.000');
        final DateTime surgeryDate =
            DateTime.parse(dateFormat.format(oldSurgery['date']));
        oldSurgeriesByDate[surgeryDate] = [];
      }
    });

    widget.oldSurgeries.forEach((oldSurgery) {
      if (oldSurgery['date'].month == month + 1 &&
          oldSurgery['date'].year == year) {
        final DateFormat dateFormat = DateFormat('yyyy-MM-dd 00:00:00.000');
        final DateTime surgeryDate =
            DateTime.parse(dateFormat.format(oldSurgery['date']));

        Map<String, dynamic> oldSurgeryData = {
          'id': oldSurgery['id'],
          'hour': oldSurgery['date'].toString().substring(11, 16),
          'name': oldSurgery['name'],
          'patientName': oldSurgery['patientName']
        };

        if (isAdmin) {
          oldSurgeryData.putIfAbsent('serial', () => oldSurgery['serial']);
          oldSurgeryData.putIfAbsent(
              'profesionalID', () => oldSurgery['profesionalID']);
        }

        oldSurgeriesByDate[surgeryDate].add(oldSurgeryData);
      }
    });

    return oldSurgeriesByDate;
  }

  void handleTap(int year, int month) {
    Map<DateTime, List<Map<String, dynamic>>> oldSurgeriesByDate =
        sortOldSurgeriesByDate(year, month);

    Navigator.of(context).push(MaterialPageRoute<void>(
        builder: (BuildContext context) => Scaffold(
              body: SurgeriesList(
                  title: 'Cirugías de ${Constants.MONTHS[month]} del $year',
                  oldSurgeriesByDate: oldSurgeriesByDate),
            )));
  }

  List<int> listOfYears() {
    List<int> listOfYears = List<int>();

    for (var surgery in widget.oldSurgeries) {
      final int year = surgery['date'].year;
      if (!listOfYears.contains(year)) listOfYears.add(year);
    }
    listOfYears.sort((a, b) => b.compareTo(a));

    return listOfYears;
  }

  List<int> getMonthsByYear(int year) {
    List<int> months = List<int>();

    for (var oldSurgery in widget.oldSurgeries) {
      final int yearOfSurgery = oldSurgery['date'].year;
      final int monthOfSurgery = oldSurgery['date'].month - 1;
      if (yearOfSurgery == year && !months.contains(monthOfSurgery))
        months.add(monthOfSurgery);
    }

    return months;
  }

  Iterable<ListTile> tiles(int year) =>
      getMonthsByYear(year).map<ListTile>((int month) => ListTile(
            title: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.0),
                child: Text(
                  Constants.MONTHS[month],
                  style: TextStyle(fontSize: 20.0),
                )),
            onTap: () => handleTap(year, month),
          ));

  List<Widget> listOfMonths(Iterable<ListTile> tiles) =>
      ListTile.divideTiles(context: context, tiles: tiles).toList();

  ExpansionTile expandableYear(int year) => ExpansionTile(
      title: Text('$year', style: TextStyle(fontSize: 25.0)),
      children: <Widget>[Column(children: listOfMonths(tiles(year)))]);

  Column oldSurgeriesExpandable() {
    List<int> listYears = listOfYears();
    List<ExpansionTile> yearTiles = List<ExpansionTile>();

    for (var year in listYears) yearTiles.add(expandableYear(year));

    return Column(children: yearTiles);
  }

  LayoutBuilder layout(Widget child) => LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constrains) =>
          SingleChildScrollView(
              child: ConstrainedBox(
                  constraints: BoxConstraints(minHeight: constrains.maxHeight),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[child],
                  ))));

  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text('Cirugías pasadas'),
          backgroundColor: Constants.MAIN_COLOR,
        ),
        body: layout(oldSurgeriesExpandable()),
      );
}
