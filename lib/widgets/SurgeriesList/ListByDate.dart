import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

import 'package:frontend/widgets/SurgeryDetails/SurgeryDetails.dart';

import 'package:frontend/graphql/Mutations.dart' as Mutations;
import 'package:frontend/constants.dart' as Constants;

class ListByDate extends StatelessWidget {
  final DateTime date;
  final List surgeries;
  final bool isAdmin;
  final bool isOld;

  ListByDate({Key key, this.date, this.surgeries, this.isAdmin, this.isOld})
      : super(key: key);

  void handleTapTile(BuildContext context, String surgeryID) {
    Navigator.of(context)
        .push(MaterialPageRoute<void>(builder: (BuildContext context) {
      return Scaffold(
        body: SurgeryDetails(surgeryID: surgeryID),
      );
    }));
  }

  Container dateHeader() => Container(
      decoration: BoxDecoration(
        color: const Color(0xAA0078A7),
      ),
      child: ListTile(
        title: Text(
          '${date.day} de ${Constants.MONTHS[date.month - 1]} del ${date.year}',
          style: TextStyle(
              fontWeight: FontWeight.bold, fontSize: 20.0, color: Colors.white),
        ),
      ));

  Future<bool> confirmDismiss(BuildContext context, DismissDirection direction,
      RunMutation runMutation, String id) async {
    final bool res = await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Borrar cirugía"),
          content: Text("Desea elimiar la programación de esta cirugía"),
          actions: <Widget>[
            FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: const Text("CANCELAR")),
            FlatButton(
                onPressed: () => Navigator.of(context).pop(true),
                child: Text("BORRAR")),
          ],
        );
      },
    );

    if (res ?? false) runMutation({'id': id});
    return false;
  }

  Mutation pendingSurgery(BuildContext context, Map<String, dynamic> surgery) {
    final List<String> keys =
        isAdmin ? ['serial', 'profesionalID'] : ['name', 'patientName'];
    final Container background = Container(
      color: Colors.red,
      child: Icon(Icons.delete_outline),
    );

    return Mutation(
      options: MutationOptions(documentNode: gql(Mutations.DELETE_SURGERY)),
      builder: (RunMutation runMutation, QueryResult queryResult) {
        return Dismissible(
          key: Key(surgery['id']),
          child: Container(
              child: ListTile(
            title: Text('${surgery['hour']} - ${surgery[keys[0]]}',
                style: TextStyle(fontSize: 20.0)),
            subtitle:
                Text('${surgery[keys[1]]}', style: TextStyle(fontSize: 20.0)),
            leading: surgery['status'] == "Aceptada"
                ? Text('OO',
                    style: TextStyle(
                        fontSize: 20.0,
                        color: Colors.lightBlue[300],
                        backgroundColor: Colors.lightBlue[300]))
                : Text(''),
            onTap: () => handleTapTile(context, surgery['id']),
          )),
          background: background,
          confirmDismiss: (DismissDirection direction) =>
              confirmDismiss(context, direction, runMutation, surgery['id']),
        );
      },
    );
  }

  Container oldSurgery(BuildContext context, Map<String, dynamic> surgery) {
    final List<String> keys =
        isAdmin ? ['serial', 'profesionalID'] : ['name', 'patientName'];
    return Container(
      child: ListTile(
          title: Text('${surgery['hour']} - ${surgery[keys[0]]}',
              style: TextStyle(
                fontSize: 20.0,
              )),
          subtitle: Text('${surgery[keys[1]]}',
              style: TextStyle(
                fontSize: 20.0,
              )),
          onTap: () => handleTapTile(context, surgery['id'])),
    );
  }

  Widget build(BuildContext context) {
    final List<Widget> surgeriesTiles = surgeries
        .map<Widget>((surgery) => isOld
            ? oldSurgery(context, surgery)
            : pendingSurgery(context, surgery))
        .toList();
    surgeriesTiles.insert(0, dateHeader());

    return Column(children: surgeriesTiles);
  }
}
