import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:frontend/widgets/CircularProgressBar/CircularProgressBar.dart';
import 'package:frontend/widgets/OldSurgeries/OldSurgeries.dart';
import 'ListByDate.dart';
import 'package:frontend/provider/index.dart';

import 'package:frontend/graphql/Subscriptions.dart' as Subscriptions;
import 'package:frontend/graphql/Queries.dart' as Queries;
import 'package:frontend/constants.dart' as Constants;

class SurgeriesList extends StatefulWidget {
  final String title;
  final Map<DateTime, List<Map<String, dynamic>>> oldSurgeriesByDate;

  SurgeriesList({Key key, this.title, this.oldSurgeriesByDate})
      : super(key: key);

  SurgeriesListState createState() => SurgeriesListState();
}

class SurgeriesListState extends State<SurgeriesList> {
  List<Map<String, dynamic>> myOldSurgeries = List<Map<String, dynamic>>();
  bool isAdmin = false;

  void initState() {
    super.initState();
    isAdmin = Provider.of<States>(context, listen: false).getIsAdmin;
  }

  void pastSurgeries() {
    Navigator.of(context).push(MaterialPageRoute<void>(
        builder: (BuildContext context) => Scaffold(
              body: OldSurgeries(oldSurgeries: myOldSurgeries),
            )));
  }

  List<dynamic> sortPendingSurgeriesData(List<dynamic> pendingSurgeries) {
    Map<DateTime, List<Map<String, dynamic>>> surgeriesByDate =
        Map<DateTime, List<Map<String, dynamic>>>();

    final List<dynamic> _pendingSurgeries = pendingSurgeries
        .where((dynamic surgery) =>
            (DateTime.now().compareTo(DateTime.parse(surgery['date'])) < 0))
        .toList();

    _pendingSurgeries.forEach((surgery) {
      final DateTime date =
          DateTime.parse('${surgery['date'].substring(0, 10)} 00:00:00');
      surgeriesByDate[date] = [];
    });

    final List sortedDates = surgeriesByDate.keys.toList();
    sortedDates.sort();

    _pendingSurgeries.forEach((surgery) {
      final DateTime date =
          DateTime.parse('${surgery['date'].substring(0, 10)} 00:00:00');

      Map<String, dynamic> surgeryData = {
        'id': surgery['id'],
        'hour': surgery['date'].substring(11, 16),
        'status': surgery['status'],
        'name': surgery['name'],
        'patientName': surgery['patientName'],
      };

      if (isAdmin) {
        surgeryData.putIfAbsent('serial', () => surgery['serial']);
        surgeryData.putIfAbsent(
            'profesionalID', () => surgery['profesionalID']);
      }

      surgeriesByDate[date].add(surgeryData);
    });

    return [sortedDates, surgeriesByDate];
  }

  List<dynamic> sortOldSurgeries(List<dynamic> oldSurgeriesList) {
    List<Map<String, dynamic>> oldSurgeries = List<Map<String, dynamic>>();

    final List<dynamic> _oldSurgeriesList = oldSurgeriesList
        .where((dynamic surgery) =>
            (DateTime.now().compareTo(DateTime.parse(surgery['date'])) > 0))
        .toList();

    _oldSurgeriesList.forEach((surgery) {
      Map<String, dynamic> oldSurgeryData = {
        'id': surgery['id'],
        'date': DateTime.parse(surgery['date']),
        'name': surgery['name'],
        'patientName': surgery['patientName'],
      };

      if (isAdmin) {
        oldSurgeryData.putIfAbsent('serial', () => surgery['serial']);
        oldSurgeryData.putIfAbsent(
            'profesionalID', () => surgery['profesionalID']);
      }

      oldSurgeries.add(oldSurgeryData);
    });

    return oldSurgeries;
  }

  // If widget.queryDB is true and the logged doctor have registered past surgeries, an Icon will be displayed
  List<Widget> appBarActions() {
    bool thereAreOldSurgeries;
    try {
      thereAreOldSurgeries = myOldSurgeries[0].length != 0;
    } catch (e) {
      thereAreOldSurgeries = myOldSurgeries.length != 0;
    }

    return <Widget>[
      thereAreOldSurgeries
          ? IconButton(
              icon: Icon(Icons.history, color: Colors.white),
              onPressed: pastSurgeries)
          : null
    ].where((Object o) => o != null).toList();
  }

  Widget message() {
    final bool isAdmin = Provider.of<States>(context).getIsAdmin;
    return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          Center(
              child: Text(
            isAdmin
                ? 'No hay cirugías pendientes por revisar.'
                : 'No tienes cirugías programadas.',
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 25.0),
          ))
        ]);
  }

  Query querySurgeries() {
    final String operationName = isAdmin ? "allSurgeriesAdmin" : 'mySurgeries';
    final String operation =
        isAdmin ? Queries.ALL_SURGERIES_ADMIN : Queries.MY_SURGERIES;

    return Query(
        options: QueryOptions(documentNode: gql(operation)),
        builder: (QueryResult res,
            {VoidCallback refetch, FetchMore fetchMore}) {
          if (res.hasException) return Text(res.exception.toString());
          if (res.loading) return CircularProgressBar();

          final List<DateTime> sortedDates =
              sortPendingSurgeriesData(res.data[operationName])[0];
          final Map<DateTime, List<Map<String, dynamic>>> surgeriesByDate =
              sortPendingSurgeriesData(res.data[operationName])[1];
          final List<Map<String, dynamic>> oldSurgeries =
              sortOldSurgeries(res.data[operationName]);

          if (myOldSurgeries.length == 0)
            SchedulerBinding.instance.addPostFrameCallback((_) => setState(() =>
                myOldSurgeries =
                    oldSurgeries.length > 0 ? oldSurgeries : [{}]));

          return sortedDates.length == 0
              ? message()
              : ListView(
                  children: sortedDates
                      .map<Widget>((date) => ListByDate(
                          date: date,
                          surgeries: surgeriesByDate[date],
                          isAdmin: isAdmin,
                          isOld: false))
                      .toList());
        });
  }

  Subscription updateSurgeries() {
    final String operationName =
        isAdmin ? "subPendingSurgeries" : "subPendingSurgeriesOfDoctor";
    final String operation = isAdmin
        ? Subscriptions.SUB_PENDING_SURGERIES
        : Subscriptions.SUB_PENDING_SURGERIES_OF_DOCTOR;
    return Subscription(
      operationName,
      operation,
      builder: ({bool loading, dynamic payload, dynamic error}) {
        if (payload == null || myOldSurgeries.length == 0) {
          return querySurgeries();
        } else {
          final List<DateTime> sortedDates =
              sortPendingSurgeriesData(payload[operationName])[0];
          final Map<DateTime, List<Map<String, dynamic>>> surgeriesByDate =
              sortPendingSurgeriesData(payload[operationName])[1];

          return sortedDates.length == 0
              ? message()
              : ListView(
                  children: sortedDates
                      .map<Widget>((date) => ListByDate(
                          date: date,
                          surgeries: surgeriesByDate[date],
                          isAdmin: isAdmin,
                          isOld: false))
                      .toList());
        }
      },
    );
  }

  ListView oldSurgeries() {
    final List<DateTime> sortedDates = widget.oldSurgeriesByDate.keys.toList();
    sortedDates.sort();

    return ListView(
        children: sortedDates
            .map<Widget>((date) => ListByDate(
                date: date,
                surgeries: widget.oldSurgeriesByDate[date],
                isAdmin: isAdmin,
                isOld: true))
            .toList());
  }

  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
          backgroundColor: Constants.MAIN_COLOR,
          actions: appBarActions(),
        ),
        body: Column(children: <Widget>[
          widget.oldSurgeriesByDate == null
              ? Expanded(child: updateSurgeries())
              : Expanded(child: oldSurgeries())
        ]),
      );
}
