import 'package:path_provider/path_provider.dart';
import 'dart:async';
import 'dart:io';

class Storage {
  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/counter.txt');
  }

  Future<String> readJWT() async {
    try {
      final file = await _localFile;
      String contents = await file.readAsString();
      return contents;
    } catch (e) {
      return '';
    }
  }

  Future<File> writeJWT(String token) async {
    final file = await _localFile;
    return file.writeAsString(token);
  }
}
