import 'package:flutter/material.dart';

class States with ChangeNotifier {
  String _jwtoken = "";
  DateTime daySelected = DateTime.now();
  String selectedHour = "";
  List<dynamic> allSurgeries = List<dynamic>();
  List<dynamic> allDocData = List<dynamic>();
  Map<String, dynamic> doctorData = Map<String, dynamic>();
  bool isAdmin = false;
  Map<DateTime, List> jornada = Map<DateTime, List>();

  String get getJwtoken => _jwtoken;
  DateTime get getDaySelected => daySelected;
  String get getSelectedHour => selectedHour;
  Map<String, dynamic> get getDoctorData => doctorData;
  List<dynamic> get getAllSurgeries => allSurgeries;
  List<dynamic> get getAllDocData => allDocData;
  bool get getIsAdmin => isAdmin;
  Map<DateTime, List> get getJornada => jornada;

  set setJwtoken(String jwtoken) {
    this._jwtoken = jwtoken;
    notifyListeners();
  }

  set setDaySelected(DateTime day) {
    this.daySelected = day;
    notifyListeners();
  }

  set setSelectedHour(String selectedHour) {
    this.selectedHour = selectedHour;
    notifyListeners();
  }

  set setIsAdmin(bool isAdmin) {
    this.isAdmin = isAdmin;
    notifyListeners();
  }

  set setJornada(Map<DateTime, List> jornada) {
    this.jornada = jornada;
    notifyListeners();
  }

  set setAllDocData(List<dynamic> allDocData) => this.allDocData = allDocData;
  set setAllSurgeries(List<dynamic> allSurgeries) =>
      this.allSurgeries = allSurgeries;
  set setDoctorData(Map<String, dynamic> doctorData) =>
      this.doctorData = doctorData;
}
