import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';
import 'package:frontend/provider/index.dart';
import 'package:frontend/graphql/index.dart';
import 'package:frontend/storage/index.dart';
import 'package:frontend/widgets/init.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  Widget build(BuildContext context) => MultiProvider(
        providers: [ChangeNotifierProvider(create: (_) => States())],
        child: Consumer<States>(
          builder: (context, states, _) => GraphQLProvider(
              client: getClient(context),
              child: MaterialApp(
                title: 'CEOZ Calendar',
                theme: ThemeData(
                  primarySwatch: Colors.blue,
                ),
                home: Init(storage: Storage()),
              )),
        ),
      );
}
