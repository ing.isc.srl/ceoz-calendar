library mutations;

const String LOGIN = r"""
  mutation login(
    $profesionalID: String!, 
    $password: String!
  ) {
    login(
      profesionalID: $profesionalID,
      password: $password
    )
  }
""";

const String LOGOUT = r"""
  mutation {
    logout
  }
""";

const String CHANGE_PASSWORD = r"""
  mutation changePassword($newPass: String!, $oldPass: String!){
    changePassword(
      newPass: $newPass,
      oldPass: $oldPass
    )
  }
""";

const String NEW_SURGERY = r"""
  mutation newSurgery(
    $patientName: String!, 
    $name: String!, 
    $date: String!, 
    $anesthesia: String!, 
    $price: String!,
    $needLens: Boolean!,
    $dioptria: String!,
    $flexibleOrPMMA: String!,
    $needSilicon: Boolean!,
    $otherThing: String!
  ) {
    newSurgery(
      data: {
        patientName: $patientName,
        name: $name,
        date: $date,
        anesthesia: $anesthesia,
        price: $price,
        needLens: $needLens,
        dioptria: $dioptria,
        flexibleOrPMMA: $flexibleOrPMMA,
        needSilicon: $needSilicon,
        otherThing: $otherThing
      }
    )
  }
""";

const String NEW_SURGERY_BY_ADMIN = r"""
  mutation newSurgeryByAdmin(
    $patientName: String!, 
    $name: String!, 
    $date: String!, 
    $anesthesia: String!, 
    $price: String!,
    $profesionalID: String!,
    $needLens: Boolean!,
    $dioptria: String!,
    $flexibleOrPMMA: String!,
    $needSilicon: Boolean!,
    $otherThing: String!
  ) {
    newSurgeryByAdmin(
      data: {
        patientName: $patientName
        name: $name
        date: $date
        anesthesia: $anesthesia
        price: $price
        profesionalID: $profesionalID,
        needLens: $needLens,
        dioptria: $dioptria,
        flexibleOrPMMA: $flexibleOrPMMA,
        needSilicon: $needSilicon,
        otherThing: $otherThing
      }
    )
  }
""";

const String APPROVE_SURGERY = r"""
  mutation approveSurgery($id: String!) {
    approveSurgery(id: $id) 
  }
""";

const String DELETE_SURGERY = r"""
  mutation deleteSurgery($id: String!) {
    deleteSurgery(id: $id)
  }
""";

const String SAVE_SEASON = r"""
  mutation saveSeason($SecondDate: String!, $firstDate: String!) {
    saveSeason(
      firstDate: $firstDate,
      SecondDate: $SecondDate
    )
  }
""";
