library subscriptions;

const String SUB_SURGERIES = """
  subscription subSurgeries {
    subSurgeries {
      date
      duration
      profesionalID
      name
      patientName
      status
    }
  }
""";

const String SUB_PENDING_SURGERIES = """
  subscription subPendingSurgeries {
    subPendingSurgeries {
      id
      name
      serial
      profesionalID
      status
      date
      patientName
    }
  }
""";

const String SUB_PENDING_SURGERIES_OF_DOCTOR = """
  subscription subPendingSurgeriesOfDoctor {
    subPendingSurgeriesOfDoctor {
      id
      name
      status
      date
      patientName
    }
  }
""";
