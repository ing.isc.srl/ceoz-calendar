import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:frontend/provider/index.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';

getClient(BuildContext context) {
  final HttpLink httpLink = HttpLink(
      // uri: 'http://192.168.1.79:4000/graphql',
      uri: 'https://apps-ceoz.herokuapp.com/graphql',
      headers: {"authorization": Provider.of<States>(context).getJwtoken});

  final WebSocketLink webSocketLink = WebSocketLink(
      // url: 'ws://192.168.1.79:4000/subscriptions',
      url: 'wss://apps-ceoz.herokuapp.com/subscriptions',
      config: SocketClientConfig(
          autoReconnect: true,
          initPayload: {
            'headers': {
              'authorization': Provider.of<States>(context).getJwtoken
            }
          },
          delayBetweenReconnectionAttempts: Duration(seconds: 1),
          inactivityTimeout: Duration(seconds: 30)));

  final Link link = httpLink.concat(webSocketLink);

  ValueNotifier<GraphQLClient> client =
      ValueNotifier(GraphQLClient(cache: InMemoryCache(), link: link));

  return client;
}
