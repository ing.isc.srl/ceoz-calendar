library queries;

const String DOCTOR = r"""
  query doctor($profesionalID: String!) {
    doctor(profesionalID: $profesionalID) {
      profesionalID
      name
      specialty
      sub_specialty
    }
  }
""";

const String GET_DOCTORS = r"""
  query {
    getDoctors {
      profesionalID
      name
    }
  }
""";

const String SURGERY = r"""
  query surgery($id: String!) {
    surgery(id: $id) {
      name
      duration
      anesthesia
      serial
      patientName
      price
      status
      needLens
      dioptria
      flexibleOrPMMA
      needSilicon
      otherThing
    }
  }
""";

const String SURGERIES = r"""
  query {
    surgeries {
      name
      date
    }
  }
""";

const String MY_SURGERIES = r"""
  query {
    mySurgeries {
      id
      name
      date
      patientName
      status
    }
  }
""";

const String ALL_SURGERIES = r"""
  query {
    allSurgeries {
      name
      duration
    }
  }
""";

const String ALL_SURGERIES_ADMIN = r"""
  query {
    allSurgeriesAdmin {
      id
      name
      serial
      profesionalID
      status
      date
      patientName
    }
  }
""";

const String SURGERIES_BY_DATE = r"""
  query surgeriesByDate($date: String!) {
    surgeriesByDate(date: $date) {
      date
      duration
      profesionalID
      name
      patientName
      status
    }
  }
""";

const String PENDING_SURGERIES_FOR_ADMIN = r"""
  query {
    pendingSurgeries {
      id
      name
      serial
      profesionalID
      status
      date
      patientName
    }
  }
""";

const String PENDING_SURGERIES_FOR_DOCTOR = r"""
  query {
    pendingSurgeries {
      id
      name
      status
      date
      patientName
    }
  }
""";

const String GET_SEASON = r"""
  query {
    getSeason
  }
""";
